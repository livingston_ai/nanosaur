# ROS2 RVIZ is quite incompatible now with Gazebo launching so here are the workarounds

# Generate newurdf parsed version ( parsing iin RVIZ doenst work properly)
ros2 run xacro xacro src/nanosaur/nanosaur_description/urdf/nanosaur.urdf.xacro src/nanosaur/nanosaur_description/urdf/nanosaur.urdf

# Go inside this file and replace all the paths to meshes to the package finding system
# Replace this (EXAMPLE):
/home/robotdev/ros2_ws/install/nanosaur_description/share/nanosaur_description/meshes
# With this
package://nanosaur_description/meshes
