import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from rclpy.qos import ReliabilityPolicy, QoSProfile

class Move(Node):

    def __init__(self):
        
        super().__init__('move')
        # create the publisher object
        self.publisher_ = self.create_publisher(Twist, '/nanosaur/cmd_vel', 10)
        # create the subscriber object
        self.subscriber = self.create_subscription(Odometry, '/nanosaur/odom', self.odom_sub, QoSProfile(depth=10, reliability=ReliabilityPolicy.BEST_EFFORT))
        # prevent unused variable warning
        self.subscriber
        # define the timer period for 0.5 seconds
        self.timer_period = 0.5
        # define the variable to save the received info
        self.pos_x = 0
        # create a Twist message
        self.cmd = Twist()
        self.timer = self.create_timer(self.timer_period, self.move)

    def odom_sub(self,msg):
        # Save the position in the X axis
        self.pos_x = msg.pose.pose.position.x

    def move(self):
        # print the data
        self.get_logger().info('Position in X: "%s"' % str(self.pos_x))

        # Logic of move
        if self.pos_x < 3:
            self.cmd.linear.x = 0.5
        else:
            self.cmd.linear.x = 0.0
            self.cmd.angular.z = 0.0

        # Publishing the cmd_vel values to topipc
        self.publisher_.publish(self.cmd)


            
def main(args=None):
    # initialize the ROS communication
    rclpy.init(args=args)
    # declare the node constructor
    move_node = Move()       
    # pause the program execution, waits for a request to kill the node (ctrl+c)
    rclpy.spin(move_node)
    # Explicity destroy the node
    move_node.destroy_node()
    # shutdown the ROS communication
    rclpy.shutdown()

if __name__ == '__main__':
    main()