from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='nanosaur_tools',
            executable='move_node',
            output='screen'),
    ])